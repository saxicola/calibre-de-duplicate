#!/bin/env calibre-debug
# Use the exported json file remove dupliate books from a calibre library.
# First use calibre to find duplicates then export a json file of the groups
# to calibre_dups.json
# Copy that and the Calibre_Library to here
# Run this once.
# Open the trimmed file in Calibre and check it


import sys
sys.path.insert(0,'/home/mikee/Projects/calibre/src') # Path to match where you cloned it. Edit this.
import init_calibre
import calibre
db = calibre.library.db('Calibre_Thinky').new_api
import json
import os


# Load the json file.
f = open('calibre_dups.json')
data = json.load(f)
f.close()
for books in data['books_for_group']:
    print(data['books_for_group'][books][1:])
    try:
        if len(data['books_for_group'][books]) > 1: # If only one book, skip.
            db.remove_books(data['books_for_group'][books][1:])
    except:
        print("There was an error removing some books. Continuing.")
        pass

